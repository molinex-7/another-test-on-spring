# Another test on Spring

All in all it's just another brick in the wall

## All in all it's just another test on Spring

Made by Thiago Molina with technologies like Spring, JPA, Hibernate, MariaDB,
Maven, VSCode(so disgusting), curl, openJDK-1.8, Bash, Fedora...

Yeah, I'm tripping...

### Description

jesus christ, how i hate java. The watchword is performance. If you want software
performance, write it in C/C ++. Now if you want developer performance, give to
him a scripting language to work with, like PHP, Python, Ruby, JS...
Because with 10, 20 lines, He gives you an API.

Java does not bring either. The developer writes as much as in C ++, and the
performance of the software is not much better than one done in a scripting
language. Worse than Java, just C# because it's Microsoft's Java

I think what makes me more nervous about Java, is the fact that it is very
dependent on an IDE. And my business is Vim. With it I produce like an animal,
not to mention that looking at a window like this, is so relaxing

![Screenshot](img/Screenshot.png)

Whatever, after the outburst, this software was written, trying to follow the
good practices of SpringMVC, and despite not delivering the "V", since it ends
at an endpoint of an API, it does what it sets out to do. Simulates the
creation of a credit card, and validates purchases.

This is the structure directory:

    src/main/java/com/test/wex/
    ├── WexApp.java
    ├── ServletInitializer.java
    ├── domain
    │   ├── exceptions
    │   │   └── ResourceException.java
    │   ├── models
    │   │   ├── Audit.java
    │   │   ├── Card.java
    │   │   ├── CardDTO.java
    │   │   └── Sale.java
    │   ├── repositories
    │   │   ├── CardRepository.java
    │   │   └── SaleRepository.java
    │   └── services
    │       ├── interfaces
    │       │   ├── CardServiceInterface.java
    │       │   └── SaleServiceInterface.java
    │       ├── CardService.java
    │       └── SaleService.java
    └── controllers
        ├── CardController.java
        └── SaleController.java

And this project has two routes:

**POST: http://localhost:9666/issue**

To create a card

**POST: http://localhost:9666/purchases**

To validate purchases

And to facilitate/document the API, there is a directory named
**scripts/** in it we have **issue.sh** which is a curl to hit
the route **/issue** and **purchases.sh** to hit the route
**/purchases**.

In addition to a directory named **scripts/payloads/** where we
have the payloads for both routes, written in JSON

See:

    scripts/
    ├── download_spring_project.sh
    ├── issue.sh
    ├── payloads
    │   ├── issue.json
    │   └── purchases.json
    └── purchases.sh

### What you need to install

I'm using fedora, if you're not...

    $ sudo dnf -y install mariadb-server mariadb java-1.8.0-openjdk curl jq maven

Configure java and mariaDB in your environment, dodger. You don't want me to
take your hand to do this, right?

### Run it

Clone this repo, clicking the button above, or:

    $ git clone 'https://gitlab.com/molinex-7/another-test-on-spring'

Make adjustments to the file, with your user and pass to MariaDB

    application.properties

And run it, from the project root directory (the one with the pom.xml)

    $ mvn spring-boot:run

### Tip

One time at http://start.spring.io

There they create archetypes for spring designs. If you want to create
one from scratch...

To facilitate things, we have **scripts/download_spring_project.sh**
another curl to download a spring project to you.

enjoy

### Contact

For any clarification contact us

    Mail: t.molinex@gmail.com
