package com.test.wex.controllers;

import java.util.Map;

import javax.validation.Valid;

import com.test.wex.domain.exceptions.ResourceException;
import com.test.wex.domain.models.Sale;
import com.test.wex.domain.services.SaleService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SaleController {
    @Autowired
    private SaleService saleService;

    @ExceptionHandler(ResourceException.class)
    @PostMapping(value = "/purchases", headers = "Accept=application/json")
    public Sale validatePurchaseHandle(@Valid @RequestBody Map<String, Object> payload) {
        return saleService.validatePurchaseHandle(payload);
    }
}