package com.test.wex.controllers;

import java.util.Map;

import javax.validation.Valid;

import com.test.wex.domain.exceptions.ResourceException;
import com.test.wex.domain.models.CardDTO;
import com.test.wex.domain.services.CardService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CardController {
    @Autowired
    private CardService cardService;
 
    @ExceptionHandler(ResourceException.class)
    @PostMapping(value = "/issue", headers = "Accept=application/json")
    public CardDTO issueCardHandle(@Valid @RequestBody Map<String, Object> payload) {
        return cardService.issueCardHandle(payload);
    }
}