package com.test.wex.domain.services;

import java.text.SimpleDateFormat;
import java.util.Map;

import javax.transaction.Transactional;

import com.test.wex.domain.exceptions.ResourceException;
import com.test.wex.domain.models.Card;
import com.test.wex.domain.models.Sale;
import com.test.wex.domain.repositories.CardRepository;
import com.test.wex.domain.repositories.SaleRepository;
import com.test.wex.domain.services.interfaces.SaleServiceInterface;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class SaleService implements SaleServiceInterface {
    @Autowired
    private SaleRepository saleRepository;

    @Autowired
    private CardService cardService;

    @Autowired
    private CardRepository cardRepository;

    @Override
    public Sale validatePurchaseHandle(Map<String, Object> payload) {
        if (
            !payload.containsKey("cartao") 
            || !payload.containsKey("validade")
            || !payload.containsKey("cvv")
            || !payload.containsKey("estabelecimento")
            || !payload.containsKey("valor")
            || !payload.containsKey("senha")
        ) {
            throw new ResourceException(
                "Cartão, Validade, cvv, estabelecimento, valor, e senha devem ser informados"
            );
        }

        Card card = cardRepository.findByNumber((String) payload.get("cartao"));
        Sale sale = new Sale();

        final double value = Double.parseDouble((String) payload.get("valor"));
        double newAmount = 0;
        

        if (this.validateCard(payload, card, sale) != null) {
            return sale;
        }

        if (this.validateExpirationCard(payload, card, sale) != null) {
            return sale;
        }

        if (this.validateTokenCard(payload, card, sale) != null) {
            return sale;
        }

        if (this.validateKeypassCard(payload, card, sale) != null) {
            return sale;
        }

        if (this.validateAmountCard(payload, card, sale) != null) {
            return sale;
        }

        newAmount = card.getAmount() - value;

        sale.setCode("00");
        sale.setAmount(String.valueOf(newAmount));
        this.setSaleDefaultProperties(payload, sale);

        card.setAmount(newAmount);
        cardRepository.saveAndFlush(card);

        sale.setCard(card);

        return saleRepository.saveAndFlush(sale);
    }

    private Sale validateCard(Map<String, Object> payload, Card card, Sale sale) {
        if (card != null) {
            return null;
        }

        sale.setCode("01");
        sale.setMessage("Numero de cartão informado é invalido");
        this.setSaleDefaultProperties(payload, sale);
        
        return saleRepository.saveAndFlush(sale); 
    }

    private Sale validateExpirationCard(Map<String, Object> payload, Card card, Sale sale) {
        String cardDate = new SimpleDateFormat("MM/yyyy").format(card.getExpirationDate());

        if (cardDate.equals((String) payload.get("validade"))) {
            return null;
        }

        sale.setCode("02");
        sale.setMessage("A validade desse cartão expirou");
        this.setSaleDefaultProperties(payload, sale);
        
        return saleRepository.saveAndFlush(sale); 
    }

    private Sale validateTokenCard(Map<String, Object> payload, Card card, Sale sale) {
        card.setToken(cardService.defineToken(card.getNumber(), card.getExpirationDate()));

        if (card.getToken().equals((String) payload.get("cvv"))) {
            return null;
        }

        sale.setCode("03");
        sale.setMessage("O cvv desse cartão não é valido");
        this.setSaleDefaultProperties(payload, sale);
        
        return saleRepository.saveAndFlush(sale); 
    }

    private Sale validateKeypassCard(Map<String, Object> payload, Card card, Sale sale) {
        final BCryptPasswordEncoder bc = new BCryptPasswordEncoder();

        if (bc.matches((String) payload.get("senha"), card.getKeypass())) {
            return null;
        }

        sale.setCode("04");
        sale.setMessage("A senha informada é invalida");
        this.setSaleDefaultProperties(payload, sale);
        
        return saleRepository.saveAndFlush(sale);
    }

    private Sale validateAmountCard(Map<String, Object> payload, Card card, Sale sale) {
        final double value = Double.parseDouble((String) payload.get("valor"));

        if (value <= card.getAmount()) {
            return null;
        }

        sale.setCode("05");
        sale.setMessage("Seu saldo é insulficiente");
        this.setSaleDefaultProperties(payload, sale);
        
        return saleRepository.saveAndFlush(sale);
    }

    private void setSaleDefaultProperties(Map<String, Object> payload, Sale sale) {
        sale.setEstablishment((String) payload.get("estabelecimento"));
        sale.setPurchaseValue(Double.parseDouble((String) payload.get("valor")));
    }
}