package com.test.wex.domain.services.interfaces;

import java.util.Map;

import com.test.wex.domain.models.Sale;

public interface SaleServiceInterface {
    Sale validatePurchaseHandle(Map<String, Object> payload);
}