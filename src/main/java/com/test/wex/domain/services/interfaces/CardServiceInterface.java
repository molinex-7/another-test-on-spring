package com.test.wex.domain.services.interfaces;

import java.util.Map;

import com.test.wex.domain.models.CardDTO;

public interface CardServiceInterface {
    CardDTO issueCardHandle(Map<String, Object> payload);
}