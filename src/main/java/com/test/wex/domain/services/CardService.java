package com.test.wex.domain.services;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.Random;

import javax.transaction.Transactional;

import com.test.wex.domain.models.Card;
import com.test.wex.domain.models.CardDTO;
import com.test.wex.domain.repositories.CardRepository;
import com.test.wex.domain.services.interfaces.CardServiceInterface;
import com.test.wex.domain.exceptions.ResourceException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import org.modelmapper.ModelMapper;

@Service
@Transactional
public class CardService implements CardServiceInterface {
    @Autowired
    private CardRepository cardRepository;

    private static final ModelMapper modelMapper = new ModelMapper();

    @Override
    public CardDTO issueCardHandle(final Map<String, Object> payload) {
        if (!payload.containsKey("nome") || !payload.containsKey("saldo")) {
            throw new ResourceException("Nome ou saldo não informado");
        }

        return this.makeCardObject(payload);
    }

    private CardDTO makeCardObject(final Map<String, Object> payload) {
        final Random random = new Random();
        final String pass = String.format("%04d", random.nextInt(1000));
        final Card card = new Card();
        CardDTO newCard = null;

        card.setAmount(Double.parseDouble((String) payload.get("saldo")));
        card.setName((String) payload.get("nome"));

        card.setExpirationDate(this.defineExpirationDate());
        card.setKeypass(this.defineKeypass(pass));
        card.setNumber(this.defineCardNumber());

        card.setToken(this.defineToken(card.getNumber(), card.getExpirationDate()));

        cardRepository.saveAndFlush(card);

        newCard = modelMapper.map(card, CardDTO.class);
        newCard.setKeypass(pass);

        return newCard;
    }

    private Date defineExpirationDate() {
        final Calendar cal = Calendar.getInstance();
        cal.add(Calendar.YEAR, 2);

        return cal.getTime();
    }

    private String defineKeypass(final String pass) {
        final BCryptPasswordEncoder bc = new BCryptPasswordEncoder();

        return bc.encode(pass);
    }

    private String defineCardNumber() {
        final String lastNumber = cardRepository.findByNumber();
        final String bin = "736667";
        String pad = "";

        if (lastNumber == null) {
            pad = String.format("%10s", "1").replace(' ', '0');
            return bin + pad;
        }

        final String removedBin = lastNumber.replace(bin, "");
        final String number = removedBin.replace("0", "");
        int realNumber = Integer.parseInt(number);

        realNumber = realNumber + 1;

        final String newNumber = Integer.toString(realNumber);
        pad = String.format("%10s", newNumber).replace(' ', '0');

        return bin + pad;
    }

    public String defineToken(final String num, final Date exp) {
        final String strDate = new SimpleDateFormat("MM/yyyy").format(exp);
        final String bin = "736667";

        final String year = strDate.substring(3, 7);
        final String month = strDate.substring(0, 2);
        final String removedBin = num.replace(bin, "");
        final String digits = removedBin.replace("0", "");

        final int numYear = Integer.parseInt(year);
        final int numMonth = Integer.parseInt(month);
        final int numDigits = Integer.parseInt(digits);

        final int calcToken = (numYear / numMonth) * numDigits;

        String token = Integer.toString(calcToken);

        if (token.length() > 3) {
            token = token.substring(token.length() - 3, token.length());
        }

        return token;
    }
}