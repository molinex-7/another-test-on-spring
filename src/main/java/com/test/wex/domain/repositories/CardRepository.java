package com.test.wex.domain.repositories;

import com.test.wex.domain.models.Card;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface CardRepository extends JpaRepository<Card, Long> {
    @Query(
        value = "select number from cards order by id desc limit 1", 
        nativeQuery = true
    )
    String findByNumber();

    Card findByNumber(String number);
}