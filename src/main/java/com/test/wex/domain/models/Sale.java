package com.test.wex.domain.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@SuppressWarnings("serial")
@Entity
@Table(name = "sales")
public class Sale extends Audit {
    @Id
	@GeneratedValue(generator = "pessoa_generator")
	@SequenceGenerator(
		name = "pessoa_generator",
		sequenceName = "pessoa_sequence",
		initialValue = 1000
    )
    @JsonIgnore
	private Long id;

    @NotBlank
	@Size(min = 2, max = 3)
    private String code;

    @JsonIgnore
    @Column(precision=10, scale=2)
    private double purchaseValue;

    @JsonInclude(Include.NON_EMPTY)
	@Transient
    private String amount;
    
    @JsonInclude(Include.NON_EMPTY)
	@Transient
    private String message;

    @JsonIgnore
	@NotBlank
	@Size(min = 3, max = 100)
    private String establishment;

    @ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name = "card_id", nullable = true)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnore
	private Card card;

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param purchaseValue the purchaseValue to set
     */
    public void setPurchaseValue(double purchaseValue) {
        this.purchaseValue = purchaseValue;
    }

    /**
     * @return the purchaseValue
     */
    public double getPurchaseValue() {
        return purchaseValue;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(String amount) {
        this.amount = amount;
    }

    /**
     * @return the amount
     */
    public String getAmount() {
        return amount;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param establishment the establishment to set
     */
    public void setEstablishment(String establishment) {
        this.establishment = establishment;
    }

    /**
     * @return the establishment
     */
    public String getEstablishment() {
        return establishment;
    }

    /**
     * @param card the card to set
     */
    public void setCard(Card card) {
        this.card = card;
    }

    /**
     * @return the card
     */
    public Card getCard() {
        return card;
    }
}