package com.test.wex.domain.models;

import java.util.Date;

import java.text.SimpleDateFormat;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class CardDTO {
	@JsonIgnore
	private Long id;

	@NotNull
	private String name;

	@NotNull
	private String number;
	
	@NotNull
	private Date expirationDate;

	@NotNull
	private double amount;

	@NotNull
	private String keypass;

	@NotNull
	private String token;

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param number the number to set
	 */
	public void setNumber(String number) {
		this.number = number;
	}

	/**
	 * @return the number
	 */
	public String getNumber() {
		return number;
	}

	/**
	 * @param expirationDate the expirationDate to set
	 */
	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	/**
	 * @return the expirationDate
	 */
	public String getExpirationDate() {
		return new SimpleDateFormat("MM/yyyy").format(expirationDate);
	}

	/**
	 * @param amount the amount to set
	 */
	public void setAmount(double amount) {
		this.amount = amount;
	}

	/**
	 * @return the amount
	 */
	public double getAmount() {
		return amount;
	}

	/**
	 * @param keypass the keypass to set
	 */
	public void setKeypass(String keypass) {
		this.keypass = keypass;
	}

	/**
	 * @return the keypass
	 */
	public String getKeypass() {
		return keypass;
	}

	/**
	 * @param token the token to set
	 */
	public void setToken(String token) {
		this.token = token;
	}

	/**
	 * @return the token
	 */
	public String getToken() {
		return token;
	}
}