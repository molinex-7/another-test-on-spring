#!/usr/bin/env bash

curl http://localhost:9666/purchases \
    -s -XPOST \
    -H "Content-Type: application/json" \
    -d @payloads/purchases.json | jq
