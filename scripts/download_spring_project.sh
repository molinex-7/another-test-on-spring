#!/usr/bin/env bash

curl https://start.spring.io/starter.tgz \
    -d applicationName="WexApp" \
    -d artifactId="wex" \
    -d description="A simple App to issue card and validate purchases" \
    -d groupId="com.test" \
    -d name="wex" \
    -d packageName="com.test.wex" \
    -d dependencies=web,data-jpa,mysql \
    | tar -xzvf -
