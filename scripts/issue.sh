#!/usr/bin/env bash

curl http://localhost:9666/issue \
    -s -XPOST \
    -H "Content-Type: application/json" \
    -d @payloads/issue.json | jq
